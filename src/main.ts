import { Group} from "./Group";
import './style.css';


const persons = [{ name: "Alice" }, { name: "Anatole" }, { name: "Armand" }, { name: "Achille" }];

const group = new Group(persons);

const appDiv = document.getElementById('app');

group.render(); 

const pageTitle = document.createElement('h1');
pageTitle.textContent = 'Budget à partager';
appDiv?.appendChild(pageTitle);

const divMain = document.createElement('div');
divMain.classList.add('divMain');
const groupForm = document.createElement('form');
const groupNameInput = document.createElement('input');
groupNameInput.type = 'text';
groupNameInput.placeholder = 'Nom du groupe';
const createGroupButton = document.createElement('button');
createGroupButton.textContent = 'Créer un groupe';

groupForm.append(divMain);
groupForm.appendChild(groupNameInput);
groupForm.appendChild(createGroupButton);
appDiv?.appendChild(groupForm);

// Ajoutez un gestionnaire d'événement pour le formulaire
groupForm.addEventListener('submit', (e) => {
  e.preventDefault();
  const groupName = groupNameInput.value;
  if (groupName) {
    // Créez un nouveau groupe avec le nom entré
    const newGroup = new Group(persons);
    newGroup.createGroup(groupName, []);
    groupNameInput.value = '';
    appDiv?.appendChild(newGroup.render());
  }

});

