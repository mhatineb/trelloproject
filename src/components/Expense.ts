
import { Person } from "../Person";

export interface Expense {
    description: string;
    amount: number;
    
    owner: Person;
}