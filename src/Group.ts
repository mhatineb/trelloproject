import { Person } from "./Person";
import { Expense } from "./components/Expense";

export class Group {
  persons: Person[] = [];
  expenses: Expense[] = [];
  div: HTMLDivElement = document.createElement('div');
  formDiv: HTMLDivElement = document.createElement('div');
  expensesDiv: HTMLDivElement = document.createElement('div');
  groupName: string | undefined;
  groupNameElement: HTMLParagraphElement = document.createElement('p');
  owesDiv: HTMLDivElement = document.createElement('div');
 
  constructor(persons: Person[] = []) {
    this.persons = persons;
  
    // Créez le formulaire pour créer un groupe
    const createGroupForm = this.createGroupForm();
    this.formDiv.appendChild(createGroupForm);
     
    // Ajoutez l'élément pour afficher le nom du groupe
    this.div.appendChild(this.groupNameElement);
    
    // Créez le formulaire pour ajouter une dépense
    const createExpenseForm = this.createExpenseForm();
    this.formDiv.appendChild(createExpenseForm);
    
    this.div.appendChild(this.formDiv);
    this.div.appendChild(this.expensesDiv);
    this.div.appendChild(this.owesDiv);
  }
  

  render() {
    return this.div;
  }
 
  createGroupForm(): HTMLFormElement {
  const form = document.createElement('form');  
  const personCheckboxes = this.createPersonCheckboxes(); 
  form.appendChild(personCheckboxes); 


  form.addEventListener('submit', (e) => {
    e.preventDefault();
    
    
  });
  
  return form;
}


  createPersonCheckboxes(): HTMLDivElement {
  const checkboxesDiv = document.createElement('div');
  checkboxesDiv.classList.add('person-checkboxes');

  this.persons.forEach(person => {
    const checkboxLabel = document.createElement('label');
    const checkbox = document.createElement('input');
    checkbox.type = 'checkbox';
    checkbox.name = 'selectedPersons';
    checkbox.value = person.name;
    checkboxLabel.appendChild(checkbox);
    checkboxLabel.appendChild(document.createTextNode(person.name));
    checkboxesDiv.appendChild(checkboxLabel);
  });

  return checkboxesDiv;
} 

  createGroup(groupName: string, selectedPersons: string[]) {
  this.groupName = groupName;
  const membersText = selectedPersons.length > 0 ? ` (Membres: ${selectedPersons.join(', ')})` : '';
  this.groupNameElement.textContent = `Nom du groupe: ${groupName}${membersText}`;
}

  addPersonToGroup(person: Person) {
  this.persons.push(person);
}

  addPerson(person: Person) {
    this.persons.push(person);
}
  
  createExpenseForm(): HTMLFormElement {
    const form = document.createElement('form');

    const nameLabel = document.createElement('label');
    nameLabel.textContent = 'Nom de la dépense: ';
    const nameInput = document.createElement('input');
    nameInput.type = 'text';
    nameLabel.appendChild(nameInput);

    const amountLabel = document.createElement('label');
    amountLabel.textContent = 'Montant de la dépense: ';
    const amountInput = document.createElement('input');
    amountInput.type = 'number';
    amountLabel.appendChild(amountInput);

    const ownerLabel = document.createElement('label');
    ownerLabel.textContent = 'Qui a payé?: ';
    const ownerSelect = document.createElement('select');

    this.persons.forEach(person => {
      const option = document.createElement('option');
      option.value = person.name;
      option.textContent = person.name;
      ownerSelect.appendChild(option);
    });

    ownerLabel.appendChild(ownerSelect);

    const addButton = document.createElement('button');
    addButton.textContent = 'Ajouter la dépense';

    form.appendChild(nameLabel);
    form.appendChild(amountLabel);
    form.appendChild(ownerLabel);
    form.appendChild(addButton);

    form.addEventListener('submit', (e) => {
      e.preventDefault();
      const name = nameInput.value;
      const amount = Number(amountInput.value);
      const ownerName = ownerSelect.value;

      const owner = this.persons.find(person => person.name === ownerName);
      if (owner) {
        const expense: Expense = {
          description: name,
          amount: amount,
          owner: owner,
        };
        this.addExpense(expense);
        this.renderExpenses();
        this.calculateOwes();
        nameInput.value = '';
        amountInput.value = '';
      } else {
        alert('Une erreur s\'est produite');
      }
    });

    return form;
  }

  addExpense(expense: Expense) {
    this.expenses.push(expense);
  }

  renderExpenses() {
    this.expensesDiv.innerHTML = "Dépenses :";
    let display: string[] = [];
    for (const item of this.expenses) {
      display.push(`${item.description} ${item.amount}€ - ${item.owner.name}`);
    }
    for (const item of display) {
      const line = document.createElement("p");
      line.textContent = item;
      this.expensesDiv.append(line);
    }
  }

  calculateOwes() {
    const balances: { [key: string]: number } = {};

    this.expenses.forEach(expense => {
      const owner = expense.owner.name;
      if (!balances[owner]) {
        balances[owner] = 0;
      }
      balances[owner] += expense.amount;
    }); 
    
    const totalExpenses = Object.values(balances).reduce((total, amount) => total + amount, 0);
    const averageExpense = totalExpenses / this.persons.length;
    const summary: string[] = [];
    for (const person of this.persons) {
      const name = person.name;
      if (!balances[name]) {
        balances[name] = 0;
      }
      const balance = balances[name] - averageExpense;
      if (balance > 0) {
        summary.push(`${name} a payé €${balance.toFixed(2)}`);
      } else if (balance < 0) {
        summary.push(`${name} doit €${Math.abs(balance).toFixed(2)}`);
      }
    }
    this.renderOwes(summary);
}

  renderOwes(summary: string[]) {
    this.owesDiv.innerHTML = "Equilibre :";
    for (const item of summary) {
      const line = document.createElement("p");
      line.textContent = item;
      this.owesDiv.append(line);
    }
  }
}